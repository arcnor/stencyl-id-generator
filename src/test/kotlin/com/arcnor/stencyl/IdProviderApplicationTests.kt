package com.arcnor.stencyl

import org.junit.Assert
import org.junit.Test
import java.util.Collections
import java.util.HashSet

class IdProviderApplicationTests {
	@Test
	fun testMultithreading() {
		val nThreads = 2000
		val iterations = 2500

		val stressTester = MultithreadedStressTester(nThreads, iterations)
		val app = IdProviderServlet()

		val projectHash = "4e28ecac4ee9c7d407ad3245f299ab6d1a744a1f"
		val resource = "lucas"

		val numbers = Collections.synchronizedSet(HashSet<Int>(nThreads * iterations))

		stressTester.stress(Runnable { numbers.add(app.generateId(projectHash, resource, 0)) })

		stressTester.shutdown()

		Assert.assertEquals(nThreads * iterations, numbers.size)
	}
}