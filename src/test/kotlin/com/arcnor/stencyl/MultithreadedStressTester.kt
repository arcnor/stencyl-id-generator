package com.arcnor.stencyl

import java.util.concurrent.CountDownLatch
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.ThreadFactory
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

class MultithreadedStressTester {
	private val executor: ExecutorService
	private val threadCount: Int
	private val iterationCount: Int


	constructor(iterationCount: Int) : this(DEFAULT_THREAD_COUNT, iterationCount) {
	}

	constructor(threadCount: Int, iterationCount: Int) {
		this.threadCount = threadCount
		this.iterationCount = iterationCount
		this.executor = Executors.newCachedThreadPool()
	}

	constructor(threadCount: Int, iterationCount: Int, threadFactory: ThreadFactory) {
		this.threadCount = threadCount
		this.iterationCount = iterationCount
		this.executor = Executors.newCachedThreadPool(threadFactory)
	}

	fun totalActionCount(): Int {
		return threadCount * iterationCount
	}

	@Throws(InterruptedException::class)
	fun stress(action: Runnable) {
		spawnThreads(action).await()
	}

	@Throws(InterruptedException::class, TimeoutException::class)
	fun blitz(timeoutMs: Long, action: Runnable) {
		if (!spawnThreads(action).await(timeoutMs, TimeUnit.MILLISECONDS)) {
			throw TimeoutException("timed out waiting for blitzed actions to complete successfully")
		}
	}

	private fun spawnThreads(action: Runnable): CountDownLatch {
		val finished = CountDownLatch(threadCount)

		for (i in 0..threadCount - 1) {
			executor.execute(Runnable {
				try {
					repeat(action)
				} finally {
					finished.countDown()
				}
			})
		}
		return finished
	}

	private fun repeat(action: Runnable) {
		for (i in 0..iterationCount - 1) {
			action.run()
		}
	}

	fun shutdown() {
		executor.shutdown()
	}

	companion object {
		/**
		 * The default number of threads to run concurrently.
		 */
		val DEFAULT_THREAD_COUNT = 2
	}
}