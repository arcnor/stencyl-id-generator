package com.arcnor.stencyl

import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicInteger
import java.util.logging.Logger
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class IdProviderServlet : HttpServlet() {
	companion object {
		private val ID_MAP = ConcurrentHashMap<String, ConcurrentHashMap<String, AtomicInteger>>()
	}

	private val LOG = Logger.getLogger(IdProviderServlet::class.java.name)

	override fun doGet(req: HttpServletRequest, resp: HttpServletResponse) {
		val pathInfo = req.pathInfo.trim('/').split('/')
		if (pathInfo.size != 3) {
			LOG.warning("Invalid path: ${req.pathInfo}")
			resp.sendError(404)
			return
		}

		val currentMax = pathInfo[2]
		try {
			val newId = generateId(pathInfo[0], pathInfo[1], currentMax.toInt())
			val writer = resp.writer
			writer.write(newId.toString())
			writer.flush()
		} catch (e:NumberFormatException) {
			LOG.warning("Invalid currentMax: '$currentMax'")
		}
	}

	fun generateId(projectHash: String,
	               resourceKind: String,
	               currentMax: Int): Int
	{
		if (projectHash.length != 40) {
			LOG.warning("Invalid hash: '$projectHash'")
			return -1
		}
		val projectMap = ID_MAP.getOrPut(projectHash, { ConcurrentHashMap() })

		val idGen = projectMap.getOrPut(resourceKind, { AtomicInteger(Math.max(0, currentMax)) })

		return idGen.incrementAndGet()
	}
}